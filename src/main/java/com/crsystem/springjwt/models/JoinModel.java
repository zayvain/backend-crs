package com.crsystem.springjwt.models;

public class JoinModel {

    private Long municipalityId;
    private String districtName;
    private String provinceName;
    private String municipalityName;
    private double municipalityServedBenes;
    private double municipalityAmountDisbursed;
    private double municipalityTargetBenes;
    private double municipalityAmountAllocated;

    public JoinModel(Long municipalityId, String districtName, String provinceName, String municipalityName, double municipalityServedBenes, double municipalityAmountDisbursed, double municipalityTargetBenes, double municipalityAmountAllocated) {
        this.municipalityId = municipalityId;
        this.districtName = districtName;
        this.provinceName = provinceName;
        this.municipalityName = municipalityName;
        this.municipalityServedBenes = municipalityServedBenes;
        this.municipalityAmountDisbursed = municipalityAmountDisbursed;
        this.municipalityTargetBenes = municipalityTargetBenes;
        this.municipalityAmountAllocated = municipalityAmountAllocated;
    }

    public Long getMunicipalityId() {
        return municipalityId;
    }

    public void setMunicipalityId(Long municipalityId) {
        this.municipalityId = municipalityId;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String ditrictName) {
        this.districtName = districtName;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getMunicipalityName() {
        return municipalityName;
    }

    public void setMunicipalityName(String municipalityName) {
        this.municipalityName = municipalityName;
    }

    public double getMunicipalityServedBenes() {
        return municipalityServedBenes;
    }

    public void setMunicipalityServedBenes(double municipalityServedBenes) {
        this.municipalityServedBenes = municipalityServedBenes;
    }

    public double getMunicipalityAmountDisbursed() {
        return municipalityAmountDisbursed;
    }

    public void setMunicipalityAmountDisbursed(double municipalityAmountDisbursed) {
        this.municipalityAmountDisbursed = municipalityAmountDisbursed;
    }

    public double getMunicipalityTargetBenes() {
        return municipalityTargetBenes;
    }

    public void setMunicipalityTargetBenes(double municipalityTargetBenes) {
        this.municipalityTargetBenes = municipalityTargetBenes;
    }

    public double getMunicipalityAmountAllocated() {
        return municipalityAmountAllocated;
    }

    public void setMunicipalityAmountAllocated(double municipalityAmountAllocated) {
        this.municipalityAmountAllocated = municipalityAmountAllocated;
    }
}
