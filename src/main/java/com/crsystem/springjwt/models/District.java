package com.crsystem.springjwt.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="district")
public class District {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 100)
    private String name;

    private String representative;
    private double targetbenes;
    private double servedbenes;
    private double amountallocated;
    private double amountdisbursed;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "province_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    @JsonProperty("province_id")
    private Province province;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTargetbenes() {
        return targetbenes;
    }

    public void setTargetbenes(double targetbenes) {
        this.targetbenes = targetbenes;
    }

    public double getServedbenes() {
        return servedbenes;
    }

    public void setServedbenes(double servedbenes) {
        this.servedbenes = servedbenes;
    }

    public double getAmountallocated() {
        return amountallocated;
    }

    public void setAmountallocated(double amountallocated) {
        this.amountallocated = amountallocated;
    }

    public double getAmountdisbursed() {
        return amountdisbursed;
    }

    public void setAmountdisbursed(double amountdisbursed) {
        this.amountdisbursed = amountdisbursed;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public String getRepresentative() {
        return representative;
    }

    public void setRepresentative(String representative) {
        this.representative = representative;
    }
}
