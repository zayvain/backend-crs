package com.crsystem.springjwt.repository;

import com.crsystem.springjwt.models.CSVformat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CSVformatRepository extends JpaRepository<CSVformat, String> {
}
