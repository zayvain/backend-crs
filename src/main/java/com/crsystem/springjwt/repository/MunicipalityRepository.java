package com.crsystem.springjwt.repository;

import com.crsystem.springjwt.models.District;
import com.crsystem.springjwt.models.Municipality;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MunicipalityRepository extends JpaRepository<Municipality, Long> {
    Page<Municipality> findByDistrictId(Long districtId, Pageable pageable);
    List<Municipality> findByDistrictId(Long districtId);
    Optional<Municipality> findByIdAndDistrictId(Long id, Long districtId);

    @Query(value = "SELECT sum(amountdisbursed) FROM Municipality where district_id = :id")
    double districtTotalAmountDisbursed(@Param("id") Long id);

    @Query(value = "SELECT sum(servedbenes) FROM Municipality where district_id = :id")
    double districtTotalServedBenes(@Param("id") Long id);
}

