package com.crsystem.springjwt.repository;

import com.crsystem.springjwt.models.JoinModel;
import com.crsystem.springjwt.models.Province;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface JoinRepository extends JpaRepository<Province, Long> {

    @Query("SELECT  new com.crsystem.springjwt.models.JoinModel(m.id,d.name,p.name,m.name,m.servedbenes,m.amountdisbursed,m.targetbenes,m.amountallocated) "
            + "FROM Municipality m LEFT JOIN m.district d LEFT JOIN d.province p")
    List<JoinModel> fetchDataLeftJoin();

    @Query("SELECT  new com.crsystem.springjwt.models.JoinModel(m.id,d.name,p.name,m.name,m.servedbenes,m.amountdisbursed,m.targetbenes,m.amountallocated) "
            + "FROM Municipality m LEFT JOIN m.district d LEFT JOIN d.province p WHERE p.id = :id")
    List<JoinModel> fetchDataLeftJoinByProvinceId(@Param("id") Long id);

    @Query("SELECT  new com.crsystem.springjwt.models.JoinModel(m.id,d.name,p.name,m.name,m.servedbenes,m.amountdisbursed,m.targetbenes,m.amountallocated) "
            + "FROM Municipality m LEFT JOIN m.district d LEFT JOIN d.province p WHERE d.id = :id")
    List<JoinModel> fetchDataLeftJoinByDistrictId(@Param("id") Long id);
}
