package com.crsystem.springjwt.repository;

import com.crsystem.springjwt.models.DBFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DBFileRepository extends JpaRepository<DBFile, String> {
//    DBFile findById(String id);
}