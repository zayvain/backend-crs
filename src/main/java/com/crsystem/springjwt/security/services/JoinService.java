package com.crsystem.springjwt.security.services;

import com.crsystem.springjwt.models.JoinModel;
import com.crsystem.springjwt.repository.JoinRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class JoinService {

    @Autowired
    private JoinRepository joinRepository;

    public List<JoinModel> leftJoinData(){
        return joinRepository.fetchDataLeftJoin();
    }

    public List<JoinModel> leftJoinDataByProvId(@PathVariable(value = "provId") Long provId){
        return joinRepository.fetchDataLeftJoinByProvinceId(provId);
    }

    public List<JoinModel> leftJoinDataByDistrictId(@PathVariable(value = "districtId") Long districtId){
        return joinRepository.fetchDataLeftJoinByDistrictId(districtId);
    }
}
