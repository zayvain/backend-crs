package com.crsystem.springjwt.security.services;

import com.crsystem.springjwt.controllers.MunicipalityController;
import com.crsystem.springjwt.models.JoinModel;
import com.crsystem.springjwt.models.Municipality;
import com.crsystem.springjwt.repository.MunicipalityRepository;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MunicipalityReportService {

    @Autowired
    private MunicipalityRepository municipalityRepository;

    @Autowired
    private JoinService joinService;

//    public String exportReport(String reportFormat) throws FileNotFoundException, JRException {
//        String path="C:\\Users\\capstonestudent\\Desktop";
//
//        List<Municipality> municipalities= municipalityRepository.findAll();
//
//        File file= ResourceUtils.getFile("classpath:municipalityreport.jrxml");
//        JasperReport jasperReport= JasperCompileManager.compileReport(file.getAbsolutePath());
//        JRBeanCollectionDataSource dataSource= new JRBeanCollectionDataSource(municipalities);
//        Map<String,Object> parameters= new HashMap<>();
//        parameters.put("createdBy","CRSystem");
//        JasperPrint jasperPrint= JasperFillManager.fillReport(jasperReport,parameters,dataSource);
//
//        if (reportFormat.equalsIgnoreCase("html")){
//            JasperExportManager.exportReportToHtmlFile(jasperPrint,path+"\\municipalityreport.html");
//        }
//        if (reportFormat.equalsIgnoreCase("pdf")){
//            JasperExportManager.exportReportToPdfFile(jasperPrint,path+"\\municipalityreport.pdf");
//        }
//
//        return "Report generated in path " + path;
//    }
    public byte[] exportReport() throws FileNotFoundException, JRException {
        byte[] bytes = null;
        List<Municipality> municipalities= municipalityRepository.findAll();

        List<JoinModel> municipalitydata = joinService.leftJoinData();


        File file= ResourceUtils.getFile("classpath:reports/municipality_report.jrxml");
        JasperReport jasperReport= JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource= new JRBeanCollectionDataSource(municipalitydata);
        Map<String,Object> parameters= new HashMap<>();
        parameters.put("createdBy","CRSystem");
        JasperPrint jasperPrint= JasperFillManager.fillReport(jasperReport,parameters,dataSource);
        bytes = JasperExportManager.exportReportToPdf(jasperPrint);

        return bytes;
    }

//    public byte[] generatePDFReport(String inputFileName, Map<String, Object> params,
//                                    JRDataSource dataSource) {
//        byte[] bytes = null;
//        JasperReport jasperReport = null;
//
//        String jrxml = storageService.loadJrxmlFile(inputFileName);
//        jasperReport = JasperCompileManager.compileReport(jrxml);
//        // Save compiled report. Compiled report is loaded next time
//        JRSaver.saveObject(jasperReport, storageService.loadJasperFile(inputFileName));
//
//        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
//        // return the PDF in bytes
//        bytes = JasperExportManager.exportReportToPdf(jasperPrint);
//
//        return bytes;
//    }
}
