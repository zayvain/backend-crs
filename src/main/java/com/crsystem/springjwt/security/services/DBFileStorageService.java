package com.crsystem.springjwt.security.services;

import com.crsystem.springjwt.exception.FileStorageException;
import com.crsystem.springjwt.exception.MyFileNotFoundException;
import com.crsystem.springjwt.models.CSVformat;
import com.crsystem.springjwt.models.DBFile;
import com.crsystem.springjwt.repository.CSVformatRepository;
import com.crsystem.springjwt.repository.DBFileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class DBFileStorageService {

    @Autowired
    private DBFileRepository dbFileRepository;

    @Autowired
    private CSVformatRepository csVformatRepository;

    public DBFile storeFile(MultipartFile file) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            DBFile dbFile = new DBFile(fileName, file.getContentType(), file.getBytes());

            return dbFileRepository.save(dbFile);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public CSVformat storeFormat(MultipartFile file) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            CSVformat csVformat = new CSVformat(fileName, file.getContentType(), file.getBytes());

            return csVformatRepository.save(csVformat);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }


    public DBFile getFile(String fileId) {
        return dbFileRepository.findById(fileId)
                .orElseThrow(() -> new MyFileNotFoundException("File not found with id " + fileId));
    }

    public CSVformat getFormat(String formatId){
        return csVformatRepository.findById(formatId)
                .orElseThrow(() -> new MyFileNotFoundException("CSV format not found with id " + formatId));
    }

    public List<DBFile> getAll(){
        return dbFileRepository.findAll();
    }

    public DBFile deleteById(String id) {
        DBFile dbFile = getFile(id);
        if(dbFile == null) {
            throw new RuntimeException("File does not exist");
        }
        dbFileRepository.deleteById(id);
        return dbFile;
    }
}
