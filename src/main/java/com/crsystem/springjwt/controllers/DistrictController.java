package com.crsystem.springjwt.controllers;

import com.crsystem.springjwt.exception.ResourceNotFoundException;
import com.crsystem.springjwt.models.District;
import com.crsystem.springjwt.repository.DistrictRepository;
import com.crsystem.springjwt.repository.ProvinceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/test/districts")
@CrossOrigin(origins = {"http://localhost:4200"})
public class DistrictController {

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private ProvinceRepository provinceRepository;


    @GetMapping("provinces/all")
    public List<District> getAll(){
        return districtRepository.findAll();
    }

    @GetMapping("/provinces/{provinceId}/districts")
    public List<District> getDisByProvId(@PathVariable(value = "provinceId") Long provinceId){
        return districtRepository.findByProvinceId(provinceId);
    }

    @GetMapping("/provinces/{provinceId}/districts/page")
    public Page<District> getAllDistrictsByProvinceId(@PathVariable(value = "provinceId") Long provinceId,
                                                      Pageable pageable) {
        return districtRepository.findByProvinceId(provinceId, pageable);
    }

    @PostMapping("/provinces/{provinceId}/districts")
    public District createDistrict(@PathVariable (value = "provinceId") Long provinceId,
                                   @Valid @RequestBody District district) {
        return provinceRepository.findById(provinceId).map(province -> {
            district.setProvince(province);
            return districtRepository.save(district);
        }).orElseThrow(() -> new ResourceNotFoundException("Province Id " + provinceId + " not found"));
    }

//    @GetMapping("/districts")
//    public Page<District> getAllDistricts(Pageable pageable) {
//        return districtRepository.findAll(pageable);
//    }
//
//    @PostMapping("/districts")
//    public District createDistrict(@Valid @RequestBody District district) {
//        return districtRepository.save(district);
//    }
//
//    @PutMapping("/districts/{districtId}")
//    public District updateDistrict(@PathVariable Long districtId, @Valid @RequestBody District postRequest) {
//        return districtRepository.findById(districtId).map(district -> {
//            district.setName(postRequest.getName());
//            district.setServedbenes(postRequest.getServedbenes());
//            district.setTargetbenes(postRequest.getTargetbenes());
//            return districtRepository.save(district);
//        }).orElseThrow(() -> new ResourceNotFoundException("DistrictId " + districtId + " not found"));
//    }
//
//
//    @DeleteMapping("/districts/{districtId}")
//    public ResponseEntity<?> deleteDistrict(@PathVariable Long districtId) {
//        return districtRepository.findById(districtId).map(district -> {
//            districtRepository.delete(district);
//            return ResponseEntity.ok().build();
//        }).orElseThrow(() -> new ResourceNotFoundException("DistrictId " + districtId + " not found"));
//    }
}
