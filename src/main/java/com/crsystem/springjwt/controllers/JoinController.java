package com.crsystem.springjwt.controllers;

import com.crsystem.springjwt.models.JoinModel;
import com.crsystem.springjwt.security.services.JoinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/test/jointable")
@CrossOrigin(origins = {"http://localhost:4200"})
public class JoinController {

    @Autowired
    private JoinService joinService;

    @GetMapping("/left")
    public ResponseEntity<List<JoinModel>> getLeftJoinData() {
        return new ResponseEntity<List<JoinModel>>(joinService.leftJoinData(), HttpStatus.OK);
    }

    @GetMapping("/byprovince/{provId}")
    public ResponseEntity<List<JoinModel>> getLeftJoinDataByProvince(@PathVariable(value = "provId") Long provId) {
        return new ResponseEntity<List<JoinModel>>(joinService.leftJoinDataByProvId(provId), HttpStatus.OK);
    }

    @GetMapping("/bydistrict/{districtId}")
    public ResponseEntity<List<JoinModel>> getLeftJoinDataByDistrict(@PathVariable(value = "districtId") Long districtId) {
        return new ResponseEntity<List<JoinModel>>(joinService.leftJoinDataByDistrictId(districtId), HttpStatus.OK);
    }
}
