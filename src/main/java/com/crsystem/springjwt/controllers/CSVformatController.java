package com.crsystem.springjwt.controllers;

import com.crsystem.springjwt.models.CSVformat;
import com.crsystem.springjwt.models.DBFile;
import com.crsystem.springjwt.payload.response.UploadFileResponse;
import com.crsystem.springjwt.security.services.DBFileStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/test/csvformat")
@CrossOrigin(origins = {"http://localhost:4200"})
public class CSVformatController {

    private static final Logger logger = LoggerFactory.getLogger(CSVformatController.class);

    @Autowired
    private DBFileStorageService dbFileStorageService;

    @PostMapping("/uploadFile")
    public UploadFileResponse uploadFormat(@RequestParam("file") MultipartFile file) {
        CSVformat csVformat = dbFileStorageService.storeFormat(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(csVformat.getId())
                .toUriString();

        return new UploadFileResponse(csVformat.getFileName(), fileDownloadUri,
                file.getContentType(), file.getSize());
    }

    @GetMapping("/downloadFile/{fileId}")
    public ResponseEntity<Resource> downloadFormat(@PathVariable String fileId) {
        // Load file from database
        CSVformat csVformat = dbFileStorageService.getFormat(fileId);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(csVformat.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + csVformat.getFileName() + "\"")
                .body(new ByteArrayResource(csVformat.getData()));
    }

    @GetMapping(value = "/{id}")
    public CSVformat findById(@PathVariable final String id) {
        return dbFileStorageService.getFormat(id);
    }
}
